import { createServer } from 'http';
import staticHandler from 'serve-handler';
import ws, { WebSocketServer } from 'ws';
let count = 0
//serve static folder
const server=createServer((req,res)=>{   // (1)
    return staticHandler(req,res,{public: 'public'})
});

const wss=new WebSocketServer({server}) // (2)
wss.on('connection',(client)=>{
    console.log('A new Client connected !')
    count++
    let clientID = "User" + count
    broadcast("[SVR] A user has started a session! Session #" + count + " during our runtime! Say Hello!")

    client.on('message',(msg)=>{    // (3)
        console.log(`Message:${msg}`);
        broadcast(msg)
    })
})

function broadcast(msg) {       // (4)
    for(const client of wss.clients){
        if(client.readyState === ws.OPEN){
            client.send(msg)
        }
    }
}

server.listen(process.argv[2] || 8083,()=>{
    console.log(`server listening...`);
})
