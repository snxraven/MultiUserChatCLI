
# Discord-Linux Multi User Chatroom
The Discord-Linux Multi User Chatroom is designed to work along side the Discord-Linux API.

This client serves as a multi user chatroom using websockets. As well as allowing users to interact with their containers right within the room.

When a new user joins, they will be asked for their token, simply use /api-key to generate a token.
